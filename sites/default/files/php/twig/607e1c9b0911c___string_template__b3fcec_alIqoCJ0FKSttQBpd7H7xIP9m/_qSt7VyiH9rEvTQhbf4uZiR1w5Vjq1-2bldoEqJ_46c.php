<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__b3fcec1da220ce9227379c23094129a5d0aab13921416f783ec862ff3c240de5 */
class __TwigTemplate_626b0e90568eac4e45c9726f95a498c7ef97730d26f8a0fd86067d3cc254b518 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = [];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<p><strong>test555</strong></p>

<p><a id=\"myCheck3\" onclick=\"myFunction()\" type=\"checkbox\" /> &nbsp;</a>Al marcar esta caja acepto que soy responsable por mis comentarios y contenidos dentro y fuera de la Comunidad #XenofobiaCero, así como todos los otros términos y condiciones de uso (link).</p>

<p><a class=\"btn btn-light\" href=\"#\" id=\"text3\">Responder a este mensaje </a>";
    }

    public function getTemplateName()
    {
        return "__string_template__b3fcec1da220ce9227379c23094129a5d0aab13921416f783ec862ff3c240de5";
    }

    public function getDebugInfo()
    {
        return array (  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__b3fcec1da220ce9227379c23094129a5d0aab13921416f783ec862ff3c240de5", "");
    }
}
