<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__fac7f820e5fdd840817980829494b3c0663131f4405f004d58c5ec3c68f117d0 */
class __TwigTemplate_b9f7b759103ced1f3db8a356ea1ae4282d23903ba40f2a57ac2a563705e0fc65 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = [];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<p><strong>test555</strong></p>

<p><input id=\"myCheck3\" onclick=\"myFunction()\" type=\"checkbox\" /> Al marcar esta caja acepto que soy responsable por mis comentarios y contenidos dentro y fuera de la Comunidad #XenofobiaCero, así como todos los otros términos y condiciones de uso (link).</p>

<p><a class=\"btn btn-light\" href=\"#\" id=\"text3\">Responder a este mensaje </a>";
    }

    public function getTemplateName()
    {
        return "__string_template__fac7f820e5fdd840817980829494b3c0663131f4405f004d58c5ec3c68f117d0";
    }

    public function getDebugInfo()
    {
        return array (  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__fac7f820e5fdd840817980829494b3c0663131f4405f004d58c5ec3c68f117d0", "");
    }
}
